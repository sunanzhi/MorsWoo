<?php
namespace MorsWoo\Exception\User;

use MorsWoo\Exception\LogicException;

class UserException extends LogicException
{
    protected $code = 20000;

    const userNotExist = '用户不存在';
    const userNotExistCode = 20001;
    const userNotExistCodeMsg = 'userNotExist';

    const passwordIncorrect = '密码不正确';
    const passwordIncorrectCode = 20002;
    const passwordIncorrectCodeMsg = 'passwordIncorrect';

}