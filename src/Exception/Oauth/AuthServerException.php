<?php
namespace MorsWoo\Exception\Oauth;

use MorsWoo\Exception\LogicException;

class AuthServerExcetion extends LogicException
{
    protected $code = 31000;
}