<?php
namespace MorsWoo\Exception;

use Exception;

/**
 * 基础逻辑异常
 */
class LogicException extends Exception
{
    protected $code = 10000;
}