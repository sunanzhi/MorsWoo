<?php
namespace MorsWoo\SDK\User;

use MorsWoo\SDK\MorsWooClient;

class User
{
    public function getUserByPassword(string $username, string $password): array
    {
        return (new MorsWooClient())->request('User/User', __FUNCTION__, false, 'array', [
            'username' => $username, 'password' => $password
        ]);
    }
}
