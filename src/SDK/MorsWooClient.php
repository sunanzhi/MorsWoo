<?php
namespace MorsWoo\SDK;

use Exception;

/**
 * class MorsWooClient
 *
 * @author sunanzhi <sunanzhi@hotmail.com>
 */
class MorsWooClient
{
    protected $moduleProviderTCP = [
        'User' => ['host' => '0.0.0.0', 'port' => 9502],
    ];

    /**
     * 命名空间
     *
     * @var string
     */
    protected $namespace;

    /**
     * 模块
     *
     * @var string
     */
    protected $module;

    /**
     * 控制器
     *
     * @var string
     */
    protected $moduleClass;

    /**
     * 请求接口
     *
     * @param string $url 请求接口路径
     * @param string $method 请求接口方法
     * @param mixed ...$args 参数
     * @return mixed
     *
     * @author sunanzhi <sunanzhi@hotmail.com>
     */
    public function request(string $url, string $method, bool $async, string $returnType, array $params = [])
    {
        // 获取api参数
        $apiParams = $this->getParams($url, $method);
        // 封装请求参数
        $requestParams = $this->packageArgs($apiParams, $params);

        // 获取配置
        $moduleConfig = $this->moduleProviderTCP[$this->module];

        $client = new \Swoole\Coroutine\Client(SWOOLE_TCP);
        if (!$client->connect($moduleConfig['host'], $moduleConfig['port'], 60)) {
            throw new Exception("connect failed. Error: {$client->errCode}\n");
        }
        $str = json_encode([
            'module' => $this->module,
            'controller' => $this->moduleClass, 
            'action' => $method, 
            'params' => $requestParams,
            'returnType' => $returnType,
        ]);
        $client->send($this->encode($str));
        //服务器已经做了pack处理
        $data = $client->recv();
            
        //未处理数据,前面有4 (因为pack 类型为N)个字节的pack
        return $this->bodyToObject(json_decode($this->decode($data), true)); 
    }

    /**
     * 获取接口参数
     *
     * @param string $url 请求接口路径
     * @param string $method 请求接口方法
     * @return array
     *
     * @author sunanzhi <sunanzhi@hotmail.com>
     */
    private function getParams(string $url, string $method): array
    {
        // 当前命名空间
        $this->namespace = (new \ReflectionClass(__CLASS__))->getNamespaceName();
        // 获取模块
        $urlExplode = explode('/', $url);
        $this->module = $urlExplode[0];
        $this->moduleClass = $urlExplode[1];
        // 获取接口参数
        $apiParams = array_column((new \ReflectionMethod($this->namespace . '\\' . $this->module . '\\' . $this->moduleClass, $method))->getParameters(), 'name');

        return $apiParams;
    }

    /**
     * 封装参数
     *
     * @param array $apiParams 接口参数
     * @param array $params 请求的参数
     * @return array
     *
     * @author sunanzhi <sunanzhi@hotmail.com>
     */
    private function packageArgs(array $apiParams, array $params): array
    {
        $resParams = [];
        $i = 0;
        foreach ($apiParams as $v) {
            $resParams[$v] = $params[$v];
            $i++;
        }
        return $resParams;
    }

    /**
     * @param array $body
     *
     * @return mixed|string|int
     */
    private function bodyToObject(array $body)
    {
        $status = 1;
        isset($body['returnType']) && ($status <<= 1)
        && (!\in_array($body['returnType'], ['int', 'integer', 'float', 'double', 'string', 'boolean', 'bool', 'array']) && class_exists($body['returnType'])) && ($status <<= 1)
        && is_subclass_of($body['returnType'], Exception::class) && ($status <<= 1)
        && isset($body['context']) && ($status <<= 1);

        switch ($status) {
            case 2:
                $object = $body['data'];
                settype($object, $body['returnType']);
                break;
            case 4:
                $object = $body['returnType']::hydractor($body['data']);
                break;
            case 8:
                throw new $body['returnType']($body['codeMsg']);
                break;
            case 16:
                throw new $body['returnType']($body['codeMsg']);
                break;
            case 1:
                $object = (string) $body;
                break;
        }

        return $object;
    }

    private function encode($str)
    {
        return pack('N', strlen($str)) . $str;
    }

    private function decode($str)
    {
        $data = substr($str, '4');
        return $data;
    }
}
